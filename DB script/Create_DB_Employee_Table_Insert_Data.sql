CREATE DATABASE ReactVsCore
GO
USE ReactVsCore 
GO
SET ANSI_NULLS ON  
GO  
SET QUOTED_IDENTIFIER ON  
GO 
GO  
CREATE TABLE [dbo].[Employee](  
    [Id] [bigint] IDENTITY(1,1) NOT NULL,
	[EmployeeId] [varchar](MAX) NOT NULL,  
    [EmployeeName] [varchar](100) NULL,  
    [Address] [varchar](MAX) NULL,  
    [EmployeeType] [int] NULL,  
    [DepartmentId] [varchar](MAX) NULL
    CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED   
	(  
		[Id] ASC
	)
);
GO
INSERT [dbo].[Employee] ([EmployeeId], [EmployeeName], [Address], [EmployeeType], [DepartmentId]) VALUES (NEWID(), N'Employee 1', N'Address Employee 1', 1, '')  
GO  
INSERT [dbo].[Employee] ([EmployeeId], [EmployeeName], [Address], [EmployeeType], [DepartmentId]) VALUES (NEWID(), N'Employee 2', N'Address Employee 2', 2, '')  
GO  