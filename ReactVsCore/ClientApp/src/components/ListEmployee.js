"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
;
var ListEmployee = /** @class */ (function (_super) {
    __extends(ListEmployee, _super);
    function ListEmployee(props) {
        var _this = _super.call(this, props) || this;
        _this.state = { employees: [] };
        fetch('api/Employee/GetAllEmployee')
            .then(function (response) { return response.json(); })
            .then(function (emp) { return _this.setState({ employees: emp }); });
        return _this;
        //fetch('api/Employee/GetEmployees')
        //    .then(response => response.json())
        //    .then(emp => this.setState({ employees: emp }));
    }
    ListEmployee.renderEmployeeTable = function (employees) {
        return (React.createElement("table", { className: 'table table-striped' },
            React.createElement("thead", null,
                React.createElement("tr", null,
                    React.createElement("th", null, "Employee Name"),
                    React.createElement("th", null, "Address"),
                    React.createElement("th", null, "Employee Type"))),
            React.createElement("tbody", null, employees.map(function (employee) {
                return React.createElement("tr", { key: employee.employeeId },
                    React.createElement("td", null, employee.employeeName),
                    React.createElement("td", null, employee.address),
                    React.createElement("td", null, employee.employeeType));
            }))));
    };
    ListEmployee.prototype.render = function () {
        var contents = ListEmployee.renderEmployeeTable(this.state.employees);
        return (React.createElement("div", null,
            " ",
            contents,
            " "));
    };
    ListEmployee.displayName = ListEmployee.name;
    return ListEmployee;
}(React.Component));
exports.ListEmployee = ListEmployee;
//# sourceMappingURL=ListEmployee.js.map