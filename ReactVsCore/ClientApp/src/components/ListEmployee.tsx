﻿import * as React from 'react';
import { Employee } from '../../models/Employee';

interface Props {
    data: Employee;
};

export class ListEmployee extends React.Component<Props, {employees: any[]}> {
    static displayName = ListEmployee.name;

    constructor(props: Props) {
        super(props);
        this.state = { employees: [] };

        fetch('api/Employee/GetAllEmployee')
            .then(response => response.json())
            .then(emp => this.setState({ employees: emp }));
        //fetch('api/Employee/GetEmployees')
        //    .then(response => response.json())
        //    .then(emp => this.setState({ employees: emp }));
    }

    static renderEmployeeTable(employees) {
        return (
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>Employee Name</th>
                        <th>Address</th>
                        <th>Employee Type</th>
                    </tr>
                </thead>
                <tbody>
                    {employees.map(employee =>
                        <tr key={employee.employeeId}>
                            <td>{employee.employeeName}</td>
                            <td>{employee.address}</td>
                            <td>{employee.employeeType}</td>
                        </tr>
                    )}
                </tbody>
            </table>
        );
    }

    render() {
        let contents = ListEmployee.renderEmployeeTable(this.state.employees);

        return (
            <div> {contents} </div>
        );
    }
}