﻿export interface Employee {
    employeeId: string,
    employeeName: string,
    address?: string,
    employeeType?: string,
    departmentNames?: string[]
}