﻿using ReactVsCore.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ReactVsCore.Interfaces
{
    public interface IEmployeeRepository
    {
        Task<List<Employee>> GetAllEmployee();
    }
}
