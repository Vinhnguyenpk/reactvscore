﻿using ReactVsCore.Entities;
using System.Collections.Generic;

namespace ReactVsCore.Interfaces
{
    public interface IDepartmentRepository
    {
        IEnumerable<Department> GetAllDepartment();
    }
}
