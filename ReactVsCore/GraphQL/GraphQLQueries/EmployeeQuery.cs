﻿using GraphQL.Types;
using ReactVsCore.GraphQL.GraphQLTypes;
using ReactVsCore.Interfaces;

namespace ReactVsCore.GraphQL.GraphQLQueries
{
    public class EmployeeQuery : ObjectGraphType
    {
        public EmployeeQuery(IEmployeeRepository employeeRepository)
        {
            Field<ListGraphType<EmployeeType>>(
                "employees",
                resolve: context => employeeRepository.GetAllEmployee()
                );
        }
    }
}
