﻿using GraphQL.Types;
using ReactVsCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactVsCore.GraphQL.GraphQLTypes
{
    public class DepartmentType: ObjectGraphType<Department>
    {
        public DepartmentType()
        {
            Field(x=>x.DepartmentId, type: typeof(IdGraphType)).Description("DepartmentId property from the Department object.");
            Field(x => x.DepartmentName).Description("DepartmentName property from the Department object.");
            Field(x => x.Description).Description("Description property from the Department object.");
        }
    }
}
