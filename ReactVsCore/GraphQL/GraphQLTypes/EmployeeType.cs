﻿using GraphQL.Types;
using ReactVsCore.Entities;

namespace ReactVsCore.GraphQL.GraphQLTypes
{
    public class EmployeeType: ObjectGraphType<Employee>
    {
        public EmployeeType()
        {
            Field(a => a.EmployeeId, type: typeof(IdGraphType));
            Field(a => a.EmployeeName);
            Field(a => a.Address);
        }
    }
}
