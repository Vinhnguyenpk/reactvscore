﻿using GraphQL;
using GraphQL.Types;
using ReactVsCore.GraphQL.GraphQLQueries;

namespace ReactVsCore.GraphQL.GraphQLSchemas
{
    public class EmployeeSchema: Schema
    {
        public EmployeeSchema(IDependencyResolver resolver): base(resolver)
        {
            Query = resolver.Resolve<EmployeeQuery>();
        }
    }
}
