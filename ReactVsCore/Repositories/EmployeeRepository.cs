﻿using Microsoft.EntityFrameworkCore;
using ReactVsCore.Entities;
using ReactVsCore.Entities.Context;
using ReactVsCore.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactVsCore.Repositories
{
    public class EmployeeRepository: IEmployeeRepository
    {
        private readonly ApplicationContext _context;

        public EmployeeRepository(ApplicationContext context)
        {
            _context = context;
        }

        public Task<List<Employee>> GetAllEmployee()
        {
            return _context.Employees.ToListAsync();
        }
    }
}
