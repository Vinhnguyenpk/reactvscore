﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GraphQL.Client;
using GraphQL.Common.Request;
using Microsoft.AspNetCore.Mvc;
using ReactVsCore.Entities;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ReactVsCore.Controllers
{
    [Route("api/[controller]")]
    public class EmployeeController : Controller
    {
        private static Employee[] Employees = new Employee[] {
            new Employee { EmployeeId = Guid.NewGuid().ToString(), EmployeeName = "Employee 1", Address = "Address Empl 1", EmployeeType = EmployeeTypes.Internship},
            new Employee { EmployeeId = Guid.NewGuid().ToString(), EmployeeName = "Employee 2", Address = "Address Empl 2", EmployeeType = EmployeeTypes.PartTime},
            new Employee { EmployeeId = Guid.NewGuid().ToString(), EmployeeName = "Employee 3", Address = "Address Empl 3", EmployeeType = EmployeeTypes.PartTime},
            new Employee { EmployeeId = Guid.NewGuid().ToString(), EmployeeName = "Employee 4", Address = "Address Empl 4", EmployeeType = EmployeeTypes.FullTime},
            new Employee { EmployeeId = Guid.NewGuid().ToString(), EmployeeName = "Employee 5", Address = "Address Empl 5", EmployeeType = EmployeeTypes.Internship},
            new Employee { EmployeeId = Guid.NewGuid().ToString(), EmployeeName = "Employee 6", Address = "Address Empl 6", EmployeeType = EmployeeTypes.FullTime},
        };

        [HttpGet("[action]")]
        public IEnumerable<Employee> GetAllEmployee()
        {
            return Employees;
        }

        [HttpGet("[action]")]
        public async Task<List<Employee>> GetEmployees()
        {
            using (GraphQLClient graphQLClient = new GraphQLClient("http://localhost:5000/graphql"))
            {
                var query = new GraphQLRequest
                {
                    Query = @"   
                        query employeesQuery{ employees   
                            { 
                                employeeId,
                                employeeName,
                                address 
                            }   
                        }",
                };
                var response = await graphQLClient.PostAsync(query);
                return response.GetDataFieldAs<List<Employee>>("employees");
            }
        }
    }
}
