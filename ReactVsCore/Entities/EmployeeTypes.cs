﻿namespace ReactVsCore.Entities
{
    public enum EmployeeTypes
    {
        PartTime,
        FullTime,
        Internship
    }
}
