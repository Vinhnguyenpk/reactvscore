﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace ReactVsCore.Entities.Context
{
    public class DepartmentContextConfiguration: IEntityTypeConfiguration<Department>
    {
        private Guid[] _ids;

        public DepartmentContextConfiguration(Guid[] ids)
        {
            _ids = ids;
        }

        public void Configure(EntityTypeBuilder<Department> builder)
        {
            builder.HasData( 
                new Department {
                    DepartmentId = _ids[0],
                    DepartmentName = "HR",
                    Description = "",
                },
                new Department
                {
                    DepartmentId = _ids[1],
                    DepartmentName = "IT",
                    Description = "",
                }
            );
        }
    }
}
