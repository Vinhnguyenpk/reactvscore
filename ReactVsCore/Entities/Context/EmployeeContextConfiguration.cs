﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace ReactVsCore.Entities.Context
{
    public class EmployeeContextConfiguration: IEntityTypeConfiguration<Employee>
    {
        private Guid[] _ids;

        public EmployeeContextConfiguration(Guid[] ids)
        {
            _ids = ids;
        }

        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder
                .HasData(
                new Employee
                {
                    EmployeeId = Guid.NewGuid().ToString(),
                    EmployeeName = "Employee 1",
                    EmployeeType = EmployeeTypes.Internship,
                    Address = "Address Employee 1",
                    DepartmentId = _ids[0]
                },
                new Employee
                {
                    EmployeeId = Guid.NewGuid().ToString(),
                    EmployeeName = "Employee 2",
                    EmployeeType = EmployeeTypes.Internship,
                    Address = "Address Employee 2",
                    DepartmentId = _ids[0]
                },
                new Employee
                {
                    EmployeeId = Guid.NewGuid().ToString(),
                    EmployeeName = "Employee 3",
                    EmployeeType = EmployeeTypes.Internship,
                    Address = "Address Employee 3",
                    DepartmentId = _ids[1]
                }
           );
        }
    }
}
