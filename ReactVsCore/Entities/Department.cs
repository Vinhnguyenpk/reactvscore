﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ReactVsCore.Entities
{
    public class Department
    {
        [Key]
        public Guid DepartmentId { get; set; }

        [Required(ErrorMessage = "Department Name is require")]
        public string DepartmentName { get; set; }

        public string Description { get; set; }

        public ICollection<Employee> Employees { get; set; }
    }
}
