﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ReactVsCore.Entities
{
    public class Employee
    {
        [Key]
        public Guid EmployeeId { get; set; }

        [Required(ErrorMessage = "Name is required")]
        public string EmployeeName { get; set; }

        public string Address { get; set; }

        public EmployeeTypes EmployeeType { get; set; }

        [ForeignKey("DepartmentId")]
        public Guid DepartmentId { get; set; }
        public Department Department { get; set; }
    }
}
